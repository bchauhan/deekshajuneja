import favicon from "../assets/icon.svg";

export const aboutWebsite = {
  "@context": "http://schema.org",
  "@type": "WebSite",
  url: "http://DeekshaJuneja.com",
  name: "Deeksha Juneja | Full-Stack Engineer, DataRobot",
  author: {
    "@type": "Person",
    name: "Deeksha Juneja",
  },
};
export const siteMeta = {
  title: "Deeksha Juneja",
  favicon: favicon,
  author: "Deeksha Juneja",
  description: "Deeksha Juneja is an MBA executive and entrepreneur",
  copyright: "Deeksha Juneja, Copyright (c) 2020",
  keywords: [
    "Deeksha Juneja",
    "deeksha",
    "deeksha juneja",
    "deeksha rotman",
    "entrepreneur",
  ],
};
